
---
title: "ajchatham.com"
date: "2019-02-27T00:00:00"
layout: "single"
---

# Welcome
This is the website of Andy Chatham (me). It runs on [IPFS](https://ipfs.io)

## Work

- I'm the founder of [DIMO](https://dimo.zone/) (Digital Infrastructure for Moving Objects)

- Past Transportation Experience: 
	- GM of California for [Transdev's](https://www.transdev.com/en/) operations contract with [Waymo](https://waymo.com/) 
	- Director of Transdev's automated shuttle projects in North America:
		- Babcock Ranch [Florida](https://www.forbes.com/sites/johnmcmanus/2018/10/22/in-autonomous-vehicles-future-garages-go-away-heres-10-big-re-impacts/#5855e23e4dc0)
		- 61AV [Denver](https://www.rtd-denver.com/projects/61av)
		- Olympic Park [Montreal](https://nextcity.org/daily/entry/autonomous-shuttles-passenger-service-montreal)
	- Product for Transdev On-Demand
		- Build dispatch and booking apps used by 10k drivers and 10m+ users for SuperShuttle, ExecuCar, and dozens of Taxi fleets (remember those?) 

	- First employee of [Project 100](https://www.bloomberg.com/news/articles/2014-03-04/las-vegas-of-all-places-may-be-about-to-reinvent-car-ownership) (later called SHIFT) an early Mobility-as-a-Service company. We built a 1MW EV Charging Hub, which at the time was the largest in the world. 

- Fellow, first class of [Venture for America](https://ventureforamerica.org/) 
	- I helped build the [Zappos.com HQ](https://officesnapshots.com/2013/12/16/new-zappos-downtown-las-vegas-headquarters/) and some other office spaces in downtown Las Vegas. 
 
 
## Skin in the Game
I am a (Small) Investor in some other projects: 
- [VoltServer](https://voltserver.com/)
- [Stumptown Whitefish](http://www.stumptowninnofwhitefish.com/)
- Web3: Filecoin, Blockstack

## Writing, Reading, Listening
- I  write on the DIMO [company blog](https://dimo.zone/writing/). Occasionally I'll post on this site if I have something to say. 
- I've been building a few [Playlists](https://open.spotify.com/user/125411272) on Spotify since 2009 

## Hacking
- Home Automation with [HomeAssistant](https://www.home-assistant.io/)
- Running [Helium](https://www.helium.com/technology) Nodes 


